/*
I3 [shape=box,color=blue,style=bold,label="John Vernou Bouvier III\nb. 1891\nd. 1957",image="images/BE037819.jpg",labelloc=b];
I4 [shape=ellipse,color=red,style=bold,label="Janet Norton Lee\nb. 2.10.1877\nd. 3.1.1968",image="images/n48862003257_1275276_1366.jpg",labelloc=b];
*/

strict digraph {
  L1  -> L11  [dir=both,penwidth=10,arrowsize=2]
  L11 -> L110 [dir=both,penwidth=10,arrowsize=2]
  L11 -> L111 [dir=both,penwidth=10,arrowsize=2]
  L11 -> L112 [dir=both,penwidth=10,arrowsize=2]
  L11 -> L113 [dir=both,penwidth=10,arrowsize=2]
  L11 -> L114 [dir=both,penwidth=10,arrowsize=2]
  L11 -> L115 [dir=both,penwidth=10,arrowsize=2]
  L1 [style=filled,fillcolor=blue,image="img/powerstation.png",penwidth=10]
  L11 [style=filled,fillcolor=cyan,image="img/home.png",penwidth=10]
  L110 [image="img/battery.png",style=filled,fillcolor=green,penwidth=10]
  L111 [image="img/pv.png",style=filled,fillcolor=green,penwidth=10]
  L112 [image="img/washer.png",style=filled,fillcolor=green,penwidth=10]
  L113 [image="img/ac.png",style=filled,fillcolor=green,penwidth=10]
  L114 [image="img/light.png",style=filled,fillcolor=green,penwidth=10]
  L115 [image="img/car.png",style=filled,fillcolor=green,penwidth=10]
  ratio=0.80
}
