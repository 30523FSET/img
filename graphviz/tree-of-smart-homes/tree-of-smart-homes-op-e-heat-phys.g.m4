define(`e_only',`')
define(`e_only',`style=invis')
define(`h_only', `')
#define(`h_only',`style=invis')

strict digraph {
  layout=circo
  bgcolor=transparent
  node [style=filled,fontsize=38,fontname="Arial-Bold",fillcolor=cyan,shape=circle,width=3]
  edge [penwidth=3,arrowsize=3]
  # Layer 1
  L0   -> L11  [dir=both]
  L0   -> L14  [dir=both]
  L0   -> L12  [dir=both]
  L0   -> L13  [dir=both]
  L0   -> L15  [dir=both]
  # Layer 2 ~Operator
  L14 -> DERSE [dir=both,e_only]
  L14 -> DERC [dir=both,e_only]
  L14 -> DERG [e_only]
  DERCHP -> L14
  L14 -> DERSH [dir=both,h_only]
  # Layer 2 House 1 ###
  L11 -> L11B [dir=both,e_only]
  L11 -> L11V [e_only]
  L11P -> L11 [e_only]
  L11 -> L11W [e_only]
  L11 -> L11A [e_only]
  L11 -> L11H
  # Layer 2 House 2 ###
  # L12 -> L12V [e_only]
  L12P -> L12 [e_only]
  L12 -> L12W [e_only]
  L12 -> L12A [e_only]
  L12 -> L12HS [dir=both,h_only]
  L12 -> L12H [h_only]
  # Layer 2 House 3 ###
  L13P -> L13 [e_only]
  L13 -> L13W [e_only]
  L13 -> L13A [e_only]
  L13 -> L13B [dir=both,e_only]
  L13 -> L13H
  # Layer 2 House 5 ###
  # L15P -> L15
  L15 -> L15W [e_only]
  L15 -> L15A [e_only]
  L15 -> L15H
  # Styles ###
  L0 [style=filled,fillcolor=gray,label="Microgrid\nHeat\nNetwork",width=5]
  L11 [style=filled,fillcolor=gray,label="Sub-network 1",width=3.5]
  L12 [style=filled,fillcolor=gray,label="Sub-network 2",width=3.5]
  L13 [style=filled,fillcolor=gray,label="Sub-network 3",width=3.5]
  L14 [style=filled,fillcolor=gray,label="Sub-network 4",width=3.5]
  L15 [style=filled,fillcolor=gray,label="Sub-network 5",width=3.5]
  DERG [style=filled,fillcolor=gold,label="PV"e_only] #,width=3.5]
  DERSE [style=filled,fillcolor=chartreuse,label="eStorage",e_only] #,width=3.5]
  DERSH [style=filled,fillcolor=orangered,label="hStorage",h_only] #,width=3.5]
  DERC [style=filled,fillcolor=gray,label="Grid",e_only] #,width=3.5]
  DERCHP [style=filled,fillcolor=orange,label="mCHP"] #,width=3.5]
  # 1
  L11B [label="eStorage",fillcolor=chartreuse,e_only]
  L11V [label="eLoad",e_only]
  L11P [label="PV",fillcolor=gold,e_only]
  L11W [label="eLoad",e_only]
  L11A [label="eLoad",e_only]
  L11H [label="[e|h]Load",fillcolor=deeppink1]
  # 2
  # L12V [label="eLoad",e_only]
  L12P [label="PV",fillcolor=gold,e_only]
  L12W [label="eLoad",e_only]
  L12A [label="eLoad",e_only]
  L12H [label="hLoad",fillcolor=red,h_only]
  L12HS [style=filled,fillcolor=orangered,label="hStorage",h_only] #,width=3.5]
  # 3
  L13W [label="eLoad",e_only]
  L13A [label="eLoad",e_only]
  L13B [label="eStorage",fillcolor=chartreuse,e_only]
  L13P [label="PV",fillcolor=gold,e_only]
  L13H [label="[e|h]Load",fillcolor=deeppink1]
  # 5
  L15W [label="eLoad",e_only]
  L15A [label="eLoad",e_only]
  L15H [label="[e|h]Load",fillcolor=deeppink1]
}
