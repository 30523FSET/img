/*
I3 [shape=box,color=blue,style=bold,label="John Vernou Bouvier III\nb. 1891\nd. 1957",image="images/BE037819.jpg",labelloc=b];
I4 [shape=ellipse,color=red,style=bold,label="Janet Norton Lee\nb. 2.10.1877\nd. 3.1.1968",image="images/n48862003257_1275276_1366.jpg",labelloc=b];
*/

strict digraph {
  LO -> L11 [dir=both, penwidth=10,arrowsize=2]
  LO -> DERG [dir=both,penwidth=10,arrowsize=2]
  LO -> L12 [dir=both,penwidth=10,arrowsize=2]
  LO -> DERS [dir=both,penwidth=10,arrowsize=2]
  LO -> L13 [dir=both, penwidth=10,arrowsize=2]
  DERC -> LO [dir=both,penwidth=10,arrowsize=2]
  L11 -> L11B [dir=both,penwidth=10,arrowsize=2]
  L11 -> L11P [dir=both,penwidth=10,arrowsize=2]
  L11 -> L11L [dir=both,penwidth=10,arrowsize=2]
  L11 -> L11V [dir=both,penwidth=10,arrowsize=2]
  L12 -> L12B [dir=both,penwidth=10,arrowsize=2]
  L12 -> L12P [dir=both,penwidth=10,arrowsize=2]
  L12 -> L12A [dir=both,penwidth=10,arrowsize=2]
  L12 -> L12L [dir=both,penwidth=10,arrowsize=2]
  L12 -> L12V [dir=both,penwidth=10,arrowsize=2]
  L13 -> L13B [dir=both,penwidth=10,arrowsize=2]
  L13 -> L13P [dir=both,penwidth=10,arrowsize=2]
  L13 -> L13A [dir=both,penwidth=10,arrowsize=2]
  L13 -> L13L [dir=both,penwidth=10,arrowsize=2]
  L13 -> L13V [dir=both,penwidth=10,arrowsize=2]
  LO [style=filled,fillcolor=red,image="img/mainframe.png",penwidth=10] /* fixedsize=true,width=5,height=5,imagescale=1] */
  DERG [style=filled,fillcolor=orangered,image="img/pv.png",penwidth=10,fixedsize=true,width=5,height=5,imagescale=1]
  DERS [style=filled,fillcolor=orangered,image="img/battery.png",penwidth=10,fixedsize=true,width=5,height=5,imagescale=1]
  DERC [style=filled,fillcolor=orangered,image="img/gen.png",penwidth=10,fixedsize=true,width=5,height=5,imagescale=1]
  L11 [style=filled,fillcolor=cyan,image="img/home.png",penwidth=10]
  L12 [style=filled,fillcolor=yellow,image="img/home.png",penwidth=10]
  L13 [style=filled,fillcolor=magenta,image="img/home.png",penwidth=10]
  L11B [image="img/pump.png",style=filled,fillcolor=green,penwidth=10]
  L12B [image="img/pump.png",style=filled,fillcolor=green,penwidth=10]
  L13B [image="img/pump.png",style=filled,fillcolor=green,penwidth=10]
  L11P [image="img/fridge.png",style=filled,fillcolor=green,penwidth=10]
  L12P [image="img/fridge.png",style=filled,fillcolor=green,penwidth=10]
  L13P [image="img/fridge.png",style=filled,fillcolor=green,penwidth=10]
  L12A [image="img/fridge.png",style=filled,fillcolor=green,penwidth=10]
  L13A [image="img/pump.png",style=filled,fillcolor=green,penwidth=10]
  L11L [image="img/light.png",style=filled,fillcolor=green,penwidth=10]
  L12L [image="img/light.png",style=filled,fillcolor=green,penwidth=10]
  L13L [image="img/light.png",style=filled,fillcolor=green,penwidth=10]
  L11V [image="img/car.png",style=filled,fillcolor=green,penwidth=10]
  L12V [image="img/car.png",style=filled,fillcolor=green,penwidth=10]
  L13V [image="img/car.png",style=filled,fillcolor=green,penwidth=10]
  layout=circo
  ratio=0.80
}
