set terminal jpeg size 1200,1000
set output 'utility-function.jpg'
set xrange[0:3]
set yrange[0:3]
e=2.78
unset key
unset xtics
unset ytics
set border
set lmargin 6
# set title "UTILITY FUNCTION" font 'arial bold,26'
set xlabel 'POWER' offset character -1, -1, -1 font 'arial bold,26'
set ylabel 'UTILITY' offset character -1, -1, -1 font 'arial bold,26'
plot 3-3*(e**(-x)) lw 8
