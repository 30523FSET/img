set terminal jpeg size 1200,1000
set output 'demand-function.jpg'
set xrange[0:4]
set yrange[0:4]
e=2.78
unset key
unset xtics
unset ytics
set border
set lmargin 6
set title "DEMAND FUNCTION" font 'arial bold,26'
set xlabel 'PRICE' offset character -1, -1, -1 font 'arial bold,26'
set ylabel 'DEMAND' offset character -1, -1, -1 font 'arial bold,26'
plot 3.5-3.5*(e**(-4+x)) lw 8 lc 2
