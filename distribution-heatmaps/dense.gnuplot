#set terminal png size 1200,1200
#set output 'hm.png'
#unset xtics
#unset ytics
#unset ztics
unset key
unset tics
unset colorbox
set xrange[0:5]
set yrange[0:5]
#set zrange[0:1]
set view 0,0,2
set pm3d
set samples 100
set isosamples 100
set hidden3d
#see show palette rgbformulae
#black dense to white
#set palette rgbformulae -3,-3,-3
a=8
e=2.718281828459
abs2(a,b)=sqrt( a**2 + b**2 )
gauss(x,y,a,b,u)=(1/(sqrt(2*pi)*u))*e**((-0.5)*(((abs2(x-a,y-b)/u)**2)))

splot \
2*2**(-a*(sqrt(0.3*(x-1)**2 + 0.3*(y-4)**2))) + \
2*2**(-a*(sqrt(0.5*(x-1)**2 + 0.5*(y-3)**2))) + \
2*2**(-a*(sqrt(1*(x-2)**2 + 1*(y-4)**2)))     + \
2*2**(-a*(sqrt(0.6*(x-4)**2 + 0.6*(y-4)**2))) + \
2*2**(-a*(sqrt(1*(x-4)**2 + 1*(y-3.3)**2)))     + \
1*2**(-a*(sqrt(1*(x-4)**2 + 1*(y-1)**2)))     + \
1*2**(-a*(sqrt(1*(x-3.5)**2 + 1*(y-1)**2)))     + \
1*2**(-a*(sqrt(1*(x-4)**2 + 1*(y-1.5)**2)))     
