strict digraph {
		a -> s [dir=both, label="electricity/money"]
		b -> s [dir=both, label="electricity/money"]
		c -> s [dir=both, label="electricity/money"]
		d -> s [dir=both, label="electricity/money"]
		e -> s [dir=both, label="electricity/money"]
		f -> s [dir=both, label="electricity/money"]
		a,b,c,d,e,f [label="AEU", style=filled, fillcolor=gray];
		s [label="Microgrid Market", style=filled, fillcolor=gray];
		splines="line";
		layout=circo;
	}
