strict digraph {
  layout=circo
  bgcolor=transparent
  node [style=filled,fontsize=38,fontname="Arial-Bold",fillcolor=cyan,shape=circle,width=3]
  edge [penwidth=3,arrowsize=3]
  # Layer 1
  L0   -> L11  [dir=both]
  L0   -> L14  [dir=both]
  L0   -> L12  [dir=both]
  L0   -> L13  [dir=both]
  L0   -> L15  [dir=both]
  # Layer 2 ~Operator
  L14 -> DERSE [color="#B4B4B4",dir=both]
  L14 -> DERC [color="#B4B4B4",dir=both]
  L14 -> DERG [color="#B4B4B4",dir=both]
  DERCHP -> L14 [color="#B4B4B4",dir=both]
  L14 -> DERSH [color="#B4B4B4",dir=both]
  # Layer 2 House 1 ###
  L11 -> L11B [color="#B4B4B4",dir=both]
  L11 -> L11V [color="#B4B4B4",dir=both]
  L11P -> L11 [color="#B4B4B4",dir=both]
  L11 -> L11W [color="#B4B4B4",dir=both]
  L11 -> L11A [color="#B4B4B4",dir=both]
  L11 -> L11H [color="#B4B4B4",dir=both]
  # Layer 2 House 2 ###
  # L12 -> L12B [color="#B4B4B4",dir=both]
  L12 -> L12V [color="#B4B4B4",dir=both]
  L12P -> L12 [color="#B4B4B4",dir=both]
  L12 -> L12W [color="#B4B4B4",dir=both]
  L12 -> L12A [color="#B4B4B4",dir=both]
  L12 -> L12H [color="#B4B4B4",dir=both]
  # Layer 2 House 3 ###
  L13P -> L13 [color="#B4B4B4",dir=both]
  L13 -> L13W [color="#B4B4B4",dir=both]
  L13 -> L13A [color="#B4B4B4",dir=both]
  L13 -> L13HS [color="#B4B4B4",dir=both]
  L13 -> L13H [color="#B4B4B4",dir=both]
  # Layer 2 House 5 ###
  # L15P -> L15
  L15 -> L15W [color="#B4B4B4",dir=both]
  L15 -> L15A [color="#B4B4B4",dir=both]
  L15 -> L15H [color="#B4B4B4",dir=both]
  # Styles ###
  L0 [style=filled,fillcolor="#585858",label="MGC",width=5]
  L11 [style=filled,fillcolor="#585858",label="RA",width=3.5]
  L12 [style=filled,fillcolor="#585858",label="RA",width=3.5]
  L13 [style=filled,fillcolor="#585858",label="RA",width=3.5]
  L14 [style=filled,fillcolor="#585858",label="RA",width=3.5]
  L15 [style=filled,fillcolor="#585858",label="RA",width=3.5]
  DERG [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  DERSE [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  DERSH [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  DERC [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  DERCHP [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  # 1
  L11B [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L11V [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L11P [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L11W [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L11A [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L11H [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  # 2
  # L12B [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L12V [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L12P [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L12W [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L12A [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L12H [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  # 3
  L13P [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L13W [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L13A [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L13H [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L13HS [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  # 5
  L15W [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L15A [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
  L15H [color="#B4B4B4",fillcolor="#EEEEEE",label=""]
}
