#!/bin/bash
[[ -z "$1" ]] && echo "Usage: make.sh <dot-file>" && exit 1
name=$(echo "$1" | egrep -o "^[^.]*")
dot -Tjpg "$1" > "$name.jpg"
