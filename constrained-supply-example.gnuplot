set terminal jpeg size 800,600
set output 'constrained-supply-example.jpg'
set title "Constrained Supply - Which Price to Use?"
set xrange [0:2]
set yrange [0:4]
set termoption lw 2
set xlabel "PRICE" font 'arial bold'
set ylabel "QUANTITY" font 'arial bold'
plot x**2 title "Supply", -x**2 +4 title "Demand", 1 title "Constraint", \
"<echo '1 1'" title "P1" with points ls 5 lc "blue", \
"<echo '1.73205 1'" title "P2" with points ls 5 lc "black"
