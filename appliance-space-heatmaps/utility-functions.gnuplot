set terminal jpeg size 1200,1200
set output 'utility-functions.jpg'
set xrange[0:4]
set yrange[0:4]
e=2.78
unset key
unset xtics
unset ytics
set border
set lmargin 6
set title "USER LOAD POWER PREFERENCE" font 'arial bold,26'
set xlabel 'POWER' offset character -1, -1, -1 font 'arial bold,26'
set ylabel 'UTILITY' offset character -1, -1, -1 font 'arial bold,26'
plot 2*(e**(-0.5*x)) lw 8, 9*x*(e**(-x)) lw 8,3.5-(2-x)**2 lw 8, 3-3*(e**(-x)) lw 8
