# Demonstrate 6 curves: {smooth,steppy}x{demand,supply,demand+supply}
set terminal jpeg size 1200,960
set xrange[0:5]
e=2.7183
unset key
set grid
set border
set lmargin 6

# Demand
d0(x) = x < 1 ? 4 : x < 3 ? 6 - 2*x : 0
d1(x) = x < 2 ? 2-2*(e**(2*(x-2))) : 0
d2(x) = x < 1 ? 3 : x < 4 ? 4 - x : 0
# Supply
s0(x) = x < 1 ? 0 : x < 3 ? 2*x - 2 : 4
s1(x) = x < 3/2. ? 3*(e**(2*(x-3/2.))) : 3
# Both supply and demand
b0(x) = d0(x)/2-1
b1(x) = 3*(0.5-(1./(1+exp(8-4*x))))

# Plot demand curves ##########
set output 'demand.jpg'
set yrange[-0.5:4.5]
set title 'EXAMPLE DEMAND CURVES' font 'arial bold, 18'
set xlabel 'PRICE' offset character -1, -1, -1 font 'arial bold, 18'
set ylabel 'DEMAND' offset character -1, -1, -1 font 'arial bold, 18'
plot d0(x) lw 3, d1(x) lw 3, d2(x) lw 3

# Plot supply curves ##########
set output 'supply.jpg'
set title 'EXAMPLE SUPPLY CURVES' font 'arial bold, 18'
set xlabel 'PRICE' offset character -1, -1, -1 font 'arial bold, 18'
set ylabel 'SUPPLY' offset character -1, -1, -1 font 'arial bold, 18'
plot s0(x) lw 3, s1(x) lw 3

# Plot supply or demand curves ##########
set output 'both.jpg'
set yrange[-2.5:2.5]
set title 'SUPPLY OR DEMAND DEPENDING ON PRICE' font 'arial bold, 18'
set xlabel 'PRICE' offset character -1, -1, -1 font 'arial bold, 18'
set ylabel 'QUANTITY' offset character -1, -1, -1 font 'arial bold, 18'
plot b0(x) lw 3, b1(x) lw 3

# Plot all curves #############
set output 'all.jpg'
set xrange[0:5]
set yrange[-10:11]
set title 'ALL CURVES AND THE SUM SHOWING EQUILIBRIUM' font 'arial bold, 18'
set xlabel 'PRICE' offset character -1, -1, -1 font 'arial bold, 18'
set ylabel 'QUANTITY' offset character -1, -1, -1 font 'arial bold, 18'
plot d0(x) lw 3, d1(x) lw 3, d2(x) lw 3, -s0(x) lw 3, -s1(x) lw 3, b0(x) lw 3, b1(x) lw 3, d0(x)+d1(x)+d2(x)-s0(x)-s1(x)+b0(x)+b1(x) lw 4 lc black
