/*
I3 [shape=box,color=blue,style=bold,label="John Vernou Bouvier III\nb. 1891\nd. 1957",image="images/BE037819.jpg",labelloc=b];
I4 [shape=ellipse,color=red,style=bold,label="Janet Norton Lee\nb. 2.10.1877\nd. 3.1.1968",image="images/n48862003257_1275276_1366.jpg",labelloc=b];
*/

strict digraph {
  L0 -> L0V [dir=both,penwidth=10,arrowsize=2]
  L0 -> DERG [dir=both,penwidth=10,arrowsize=2]
  L0 -> L0W [dir=both,penwidth=10,arrowsize=2]
  L0 -> DERS [dir=both,penwidth=10,arrowsize=2]
  L0 -> L0A [dir=both,penwidth=10,arrowsize=2]
  L0 -> DERC [dir=both,penwidth=10,arrowsize=2]
  L0 -> L0L [dir=both,penwidth=10,arrowsize=2]
  L0 [style=filled,fillcolor=red,image="img/mainframe.png",penwidth=10]
  DERG [style=filled,fillcolor=green,image="img/pv.png",penwidth=10]
  DERS [style=filled,fillcolor=green,image="img/battery.png",penwidth=10]
  DERC [style=filled,fillcolor=green,image="img/transmission.png",penwidth=10]
  L0W [image="img/washer.png",style=filled,fillcolor=green,penwidth=10]
  L0A [image="img/ac.png",style=filled,fillcolor=green,penwidth=10]
  L0L [image="img/light.png",style=filled,fillcolor=green,penwidth=10]
  L0V [image="img/car.png",style=filled,fillcolor=green,penwidth=10]
  layout=circo
  ratio=0.80
}
