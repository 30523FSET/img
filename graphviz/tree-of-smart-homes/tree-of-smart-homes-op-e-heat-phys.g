


#define(`h_only',`style=invis')

strict digraph {
  layout=circo
  bgcolor=transparent
  node [style=filled,fontsize=38,fontname="Arial-Bold",fillcolor=cyan,shape=circle,width=3]
  edge [penwidth=3,arrowsize=3]
  # Layer 1
  L0   -> L11  [dir=both]
  L0   -> L14  [dir=both]
  L0   -> L12  [dir=both]
  L0   -> L13  [dir=both]
  L0   -> L15  [dir=both]
  # Layer 2 ~Operator
  L14 -> DERSE [dir=both,style=invis]
  L14 -> DERC [dir=both,style=invis]
  L14 -> DERG [style=invis]
  DERCHP -> L14
  L14 -> DERSH [dir=both,]
  # Layer 2 House 1 ###
  L11 -> L11B [dir=both,style=invis]
  L11 -> L11V [style=invis]
  L11P -> L11 [style=invis]
  L11 -> L11W [style=invis]
  L11 -> L11A [style=invis]
  L11 -> L11H
  # Layer 2 House 2 ###
  # L12 -> L12V [e_only]
  L12P -> L12 [style=invis]
  L12 -> L12W [style=invis]
  L12 -> L12A [style=invis]
  L12 -> L12HS [dir=both,]
  L12 -> L12H []
  # Layer 2 House 3 ###
  L13P -> L13 [style=invis]
  L13 -> L13W [style=invis]
  L13 -> L13A [style=invis]
  L13 -> L13B [dir=both,style=invis]
  L13 -> L13H
  # Layer 2 House 5 ###
  # L15P -> L15
  L15 -> L15W [style=invis]
  L15 -> L15A [style=invis]
  L15 -> L15H
  # Styles ###
  L0 [style=filled,fillcolor=gray,label="Microgrid\nHeat\nNetwork",width=5]
  L11 [style=filled,fillcolor=gray,label="Sub-network 1",width=3.5]
  L12 [style=filled,fillcolor=gray,label="Sub-network 2",width=3.5]
  L13 [style=filled,fillcolor=gray,label="Sub-network 3",width=3.5]
  L14 [style=filled,fillcolor=gray,label="Sub-network 4",width=3.5]
  L15 [style=filled,fillcolor=gray,label="Sub-network 5",width=3.5]
  DERG [style=filled,fillcolor=gold,label="PV"style=invis] #,width=3.5]
  DERSE [style=filled,fillcolor=chartreuse,label="eStorage",style=invis] #,width=3.5]
  DERSH [style=filled,fillcolor=orangered,label="hStorage",] #,width=3.5]
  DERC [style=filled,fillcolor=gray,label="Grid",style=invis] #,width=3.5]
  DERCHP [style=filled,fillcolor=orange,label="mCHP"] #,width=3.5]
  # 1
  L11B [label="eStorage",fillcolor=chartreuse,style=invis]
  L11V [label="eLoad",style=invis]
  L11P [label="PV",fillcolor=gold,style=invis]
  L11W [label="eLoad",style=invis]
  L11A [label="eLoad",style=invis]
  L11H [label="[e|h]Load",fillcolor=deeppink1]
  # 2
  # L12V [label="eLoad",e_only]
  L12P [label="PV",fillcolor=gold,style=invis]
  L12W [label="eLoad",style=invis]
  L12A [label="eLoad",style=invis]
  L12H [label="hLoad",fillcolor=red,]
  L12HS [style=filled,fillcolor=orangered,label="hStorage",] #,width=3.5]
  # 3
  L13W [label="eLoad",style=invis]
  L13A [label="eLoad",style=invis]
  L13B [label="eStorage",fillcolor=chartreuse,style=invis]
  L13P [label="PV",fillcolor=gold,style=invis]
  L13H [label="[e|h]Load",fillcolor=deeppink1]
  # 5
  L15W [label="eLoad",style=invis]
  L15A [label="eLoad",style=invis]
  L15H [label="[e|h]Load",fillcolor=deeppink1]
}
